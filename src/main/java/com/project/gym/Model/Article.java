package com.project.gym.Model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @Type(type="text")
    private String text;

    private String shortText;

    @Basic
    @Temporal(TemporalType.DATE)
    private Date date;

    public Article(String title, String text, String shortText) {
        this.title = title;
        this.text = text;
        this.shortText = shortText;
        Date currentDate = new Date();
        this.date = currentDate;
    }
}
