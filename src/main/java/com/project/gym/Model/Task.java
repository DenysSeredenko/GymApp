package com.project.gym.Model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String date;

    private String startTime;

    private String stopTime;


    @Column(length=1000)
    private String description;

    @ManyToOne
    @JoinColumn(name="USER_EMAIL")
    private User user;

    public Task(String date,String description) {
        this.date = date;
        this.description = description;
    }
    public Task(String date, String startTime, String stopTime, String description) {
        this.date = date;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.description = description;
    }
}
